import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class Detail extends StatefulWidget {
  String name;
  String image;
  String description;
  String videourl;

  Detail({this.name, this.image, this.description,this.videourl});


  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  initState(){
    _controller = VideoPlayerController.network(this.widget.videourl);
    //_controller = VideoPlayerController.asset("videos/sample_video.mp4");
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    super.initState();
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (_controller.value.isPlaying) {
              _controller.pause();
            } else {
              _controller.play();
            }
          });
        },
        child:
        Icon(_controller.value.isPlaying ? Icons.pause : Icons.play_arrow),
      ),
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: Text("Detail Page"),
      ),
      body: Center(
        child:SingleChildScrollView(
            child:Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.all(4),
                  padding: EdgeInsets.all(4),
                  color: Colors.white,
                  alignment: Alignment.center,
                  child: Column(
                    children: <Widget>[
                      new Text(
                        this.widget.name,
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontWeight: FontWeight.w900,
                            fontStyle: FontStyle.normal,
                            fontFamily: 'Open Sans',
                            fontSize: 40),
                      ),
                      new Image.network(this.widget.image,width: 200,height: 200,),
                      new Text(this.widget.description,
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 20)),
                      if(this.widget.videourl != null)
                          new Container(
                            margin: EdgeInsets.all(4),
                            padding: EdgeInsets.all(4),
                            color: Colors.white,
                            alignment: Alignment.center,
                            child: AspectRatio(
                              aspectRatio: _controller.value.aspectRatio,
                              child: VideoPlayer(_controller),
                            ),
                          ),

                    ],
                  ),
                )
              ],
            )
           )),
    );
  }
}
